import Task from './FromTask';
import "../index.css"
const FormTasks = ({ tasks, onDelete, onEdit }) => {
  return (
    <>
      {
        tasks.map((task) => (
          <Task key={task.id} task={task} onDelete={onDelete} onEdit={onEdit} />
          
        ))
        
      }
    </>
    )
}
export default FormTasks;
