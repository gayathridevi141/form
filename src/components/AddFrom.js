
import { useState } from 'react';
import Swal from "sweetalert2";
const AddTask = ({ onSave }) => {
    const [salutation, setSalutation] = useState('');
    const [firstname , setFirstname] = useState('');
    const [lastname , setLastname] = useState('');

    const [gender, setGender ] = useState('');
    const [email, setEmail ] = useState('');
    const [date, setDate ] = useState('');
    const [address,setAddress ] = useState('');

  

    const onSubmit = (e) => {
        e.preventDefault();
        if (!firstname && !lastname) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Fill in your from or close the form!'
            })
        } else if (!firstname && lastname) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Fill in your from!'
            })
        } else if (firstname && !lastname) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Fill in your name!'
            })
        } else {
            onSave({ salutation ,firstname, lastname , gender , email , date , address});
        }
        setFirstname('');
    setLastname ('');
   setEmail ('');
    }
   
    return (
       
        <form className="add-form" onSubmit={onSubmit}>
             <div className="form-control">
                <label>Salutation:</label>
                <select value={salutation} onChange={(e) => setSalutation(e.target.value)}>
                    <option value="None"  onChange={(e) => setSalutation(e.target.value)}>None</option>
                    <option value="eg.." onChange={(e) => setSalutation(e.target.value)} >eg..</option>
                    <option value="eg1.." onChange={(e) => setSalutation(e.target.value)}>text</option>
                    <option value="eg2.." onChange={(e) => setSalutation(e.target.value)}>example..</option>
                </select>

            </div>
            <div className="form-control">
                <label>First Name: </label>
                <input type="text" placeholder="Enter your First Name" value={firstname} onChange={(e) => setFirstname(e.target.value)} />
            </div>
            <div className="form-control">
                <label>Last Name:</label>
                <input type="text" placeholder="Enter your Last Name"   value={lastname} onChange={(e) => setLastname(e.target.value)} />
               
            </div>
            <div className="form-control" >
                <label id="gender" onChange={(e) => setSalutation(e.target.value)}>Gender: </label>
                <input type="radio" name="gender" Value="Male" defaultChecked={gender === "Male"} value={gender} onChange={e => setGender(e.target.value)} /> <label>Male</label>
                <input type="radio" name="gender" Value="Female" defaultChecked={gender === "Female"} value={gender} onChange={e => setGender(e.target.value)} /> <label>Female</label>
            </div>

            <div className="form-control">
                <label>Email: </label>
                <input type="text" placeholder="Enter your Email" value={email} onChange={(e) => setEmail(e.target.value)} />
            </div>
            <div className="form-control">
                <label>Date: </label>
                <input type="date" value={date} onChange={(e) => setDate (e.target.value)} />
            </div>
            <div className="form-control">
            <label >Address:</label>
              <textarea  placeholder="Enter your Address"  rows="5" cols="50" value={address} onChange={(e) => setAddress(e.target.value)}></textarea>

            </div>
            <input type="submit" className="btn btn-success" value="Save From" />
        </form>
    );
}
       
export default AddTask;