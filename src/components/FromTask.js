
import "../index.css"
const Task = ({ task, onDelete, onEdit }) => {
    return (
      <div>
        <div className="task">
          <div>
            <p className="taskName">
              <span className="textBold"> Salutation</span> {task.salutation}
            </p>
           
        <p className="taskDate"><span className="textBold">First Name: </span> {task.firstname}
            </p>
            
        <p className="taskDate"><span className="textBold">Last Name: </span> {task.lastname}
            </p>
           
            <p className="taskDate"><span className="textBold">Email: </span> {task.email}
            </p>
            <p className="taskDate"><span className="textBold">Date: </span> {task.date}
            </p>
            <p className="taskDate"><span className="textBold">Address </span> {task.address}
            </p>
            </div>
            <div>
         
          <button type="button" onClick={() => onEdit(task.id)}  class="btn btn-warning">Edit</button>
          <button type="button" onClick={() => onDelete(task.id)} class="btn btn-danger">Delete</button>
          </div>
        </div>
      </div>
    )
}
export default Task;