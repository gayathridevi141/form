
import Header from './components/Header';
import Tasks from './components/FromTasks';
import AddTask from './components/AddFrom';

import { useState, useEffect } from 'react';

import { v4 as uuidv4 } from 'uuid';
import Swal from "sweetalert2";
function App() {
  
   
    const [tasks, setTasks] = useState([]); 
    const [showAddFrom, setShowAddFrom] = useState(false); // add  form
    //loader..
    useEffect(() => {
        setTimeout(() => {
      
        }, 3500);
    }, [])
    //  Local Storage 
    const getFroms = JSON.parse(localStorage.getItem("fromAdded"));
    useEffect(() => {
        if (getFroms == null) {
            setTasks([])
        } else {
            setTasks(getFroms);
        }
    }, [])
    // Add From
    const addFrom = (task) => {
        const id = uuidv4();
        const newFrom = { id, ...task }
        setTasks([...tasks, newFrom]);
        Swal.fire({
            icon: 'success',
            title: 'Yay...',
            text: 'You have successfully added a new task!'
        })
        localStorage.setItem("fromAdded", JSON.stringify([...tasks, newFrom]));
    }
    // Delete From
    const deleteFrom = (id) => {
        const deleteFrom= tasks.filter((task) => task.id !== id);
        setTasks(deleteFrom);
        Swal.fire({
            icon: 'success',
            title: 'Oops...',
            text: 'You have successfully deleted a From!'
        })
        localStorage.setItem("fromAdded", JSON.stringify(deleteFrom));
    }
    // Edit From
    const editFrom = (id) => {
        const salutation = prompt("Salutation");
        const firstname = prompt("First Name");
        const lastname = prompt("Last Name");
        const email= prompt(" Email");
        const date= prompt(" Date");
        const address= prompt(" Address");
        let data = JSON.parse(localStorage.getItem('fromAdded'));
        const myData = data.map(x => {
            if (x.id === id) {
                return {
                  
                    salutation: salutation,
                    firstname: firstname,
                    lastname : lastname,
                    email:email,
                    date:date,
                    address:address,

                    id: uuidv4()
                }
            }
            return x;
        })
        Swal.fire({
            icon: 'success',
            title: 'Yay...',
            text: 'You have successfully edited!'
        })
        localStorage.setItem("fromAdded", JSON.stringify(myData));
        window.location.reload();
    }
    return (
        <>
           
                    <div className="container">
                       
                        <Header showForm={() => setShowAddFrom(!showAddFrom)} changeTextAndColor={showAddFrom} />
                    
                        {showAddFrom && <AddTask onSave={addFrom} />}
                      
                        <h3>Number of Froms {tasks.length}</h3>
                        {/* displaying Fromsdetails.. */}
                        {
                            tasks.length > 0 ?
                                (<Tasks tasks={tasks} onDelete={deleteFrom} onEdit={editFrom} />) :
                                ('No From Found!')
                        }
                    
                    </div>
            
        </>
    )
}
export default App;